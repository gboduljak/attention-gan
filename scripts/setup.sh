echo "Installing unzip..."
apt-get install unzip
echo "Installing dependencies..."
pip install pipenv
pipenv install --system
mkdir -p datasets
chmod +x ${PWD}/scripts/download_plant_pathology_2020.sh
sh ${PWD}/scripts/download_plant_pathology_2020.sh
python ${PWD}/scripts/generate_plant_pathology_2020_gan_dataset.py
