echo "Downloading PlantPathology2020..."
export KAGGLE_USERNAME=gboduljak
export KAGGLE_KEY=ebc4af434f54b3ddb76cc66ce59892bd
ORIG_DIR=${PWD}

cd ${PWD}/datasets/
kaggle competitions download -c plant-pathology-2020-fgvc7
mkdir plant-pathology-2020-fgvc7-raw
unzip plant-pathology-2020-fgvc7 -d plant-pathology-2020-fgvc7-raw
cd $ORIG_DIR